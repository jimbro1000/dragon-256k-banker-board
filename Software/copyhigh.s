; Dragon 256K Banker Board Software
; S.Orchard 2021
;
; Copies BASIC and cartridge ROMs into all four upper 32K banks in MAP1.
; With a typical 8K cartridge this provides four banks of general
; purpose memory from $e000-$feff
;
; If no cartridge is installed then the whole region from $c000-$feff
; is available.
;
; Upper banks can be selected from BASIC by POKEing to &HFF34 - &HFF37.
;
;
; Assemble with asm6809 (www.6809.org.uk/asm6809)
;
; create a DragonDos binary:
;    asm6809 -D -o COPYHIGH.BIN copyhigh.s
;


        org $4000
        
        pshs cc
        orcc #$50
        
        lda #4
        sta count
        
        ldu #$ff34
        
2       sta ,u+         ; set upper bank
        ldx #$8000      ; start of BASIC ROM

1       sta $ffde       ; map 0
        ldd ,x
        sta $ffdf       ; map 1
        std ,x++
        cmpx #$ff00     ; end of ROM
        blo 1b
        
        dec count
        bne 2b
        
        sta $ff34       ; select upper bank 0

        puls cc,pc

count   rmb 1

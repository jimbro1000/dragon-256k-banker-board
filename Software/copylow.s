; Dragon 256K Banker Board Software
; S.Orchard 2021
;
; Copies lower bank zero to the other three lower 32K banks
; giving four separate Dragon sessions.
;
; Loads high up in the cartridge area therefore
; machine must already be in map1 for this to work.
;
; Lower banks can be selected from BASIC by POKEing to &HFF30 - &HFF33.
; This works from the BASIC prompt but not in a BASIC program
; (Because the stack changes with the bank and upsets things)
;
;
; Assemble with asm6809 (www.6809.org.uk/asm6809)
;
; create a DragonDos binary:
;    asm6809 -D -o COPYLOW.BIN copylow.s
;


        org $e000
        
        pshs cc
        orcc #$50
        
        ldb #3
        ldy #$ff30      ; lower bank registers
        sta 9,y         ; fix video in bank 0 while copying
                        ; (avoids flicker)
        
2       ldx #0          ; start of RAM
1       sta ,y          ; set lower bank 0
        ldu ,x
        sta b,y         ; set destination bank
        stu ,x++
        cmpx #$8000     ; end of RAM
        blo 1b
        decb
        bne 2b
        
        sta ,y          ; select lower bank 0
        sta 8,y         ; set video bank = lower bank

        puls cc,pc


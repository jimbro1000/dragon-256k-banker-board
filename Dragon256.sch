EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Dragon 256K Banker Board"
Date "2021-04-03"
Rev ""
Comp "S.Orchard"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS132 U3
U 1 1 6066E181
P 6650 1700
F 0 "U3" H 6650 2025 50  0000 C CNN
F 1 "74LS132" H 6650 1934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6650 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 6650 1700 50  0001 C CNN
	1    6650 1700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U3
U 2 1 606754DF
P 8050 1700
F 0 "U3" H 8050 2025 50  0000 C CNN
F 1 "74LS132" H 8050 1934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 8050 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 8050 1700 50  0001 C CNN
	2    8050 1700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U3
U 3 1 60677C89
P 7350 2500
F 0 "U3" H 7350 2825 50  0000 C CNN
F 1 "74LS132" H 7350 2734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 7350 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 7350 2500 50  0001 C CNN
	3    7350 2500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U3
U 4 1 60679A5B
P 7350 1700
F 0 "U3" H 7350 2025 50  0000 C CNN
F 1 "74LS132" H 7350 1934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 7350 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 7350 1700 50  0001 C CNN
	4    7350 1700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS132 U3
U 5 1 6067B6CF
P 9650 2050
F 0 "U3" H 9880 2096 50  0000 L CNN
F 1 "74LS132" H 9880 2005 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 9650 2050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS132" H 9650 2050 50  0001 C CNN
	5    9650 2050
	1    0    0    -1  
$EndComp
$Comp
L Dragon256:74LS783 U1
U 1 1 60683F94
P 3800 1700
F 0 "U1" H 3750 1950 70  0000 C CNN
F 1 "74LS785" H 3850 1800 70  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm_Socket" H 4500 450 60  0001 C CNN
F 3 "" H 4500 450 60  0000 C CNN
	1    3800 1700
	1    0    0    -1  
$EndComp
$Comp
L Dragon256:GAL22V10 U2
U 1 1 6068753F
P 7250 3950
F 0 "U2" H 6850 4800 50  0000 C CNN
F 1 "GAL22V10" H 6950 4700 50  0000 C CNN
F 2 "Package_DIP:DIP-24_W7.62mm_Socket" H 7250 3900 50  0001 C CNN
F 3 "~" H 7250 3900 50  0001 C CNN
	1    7250 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1800 3350 1800
Wire Wire Line
	3500 1900 3350 1900
Wire Wire Line
	3500 2000 3350 2000
Wire Wire Line
	3500 2100 3350 2100
Wire Wire Line
	3500 2200 3350 2200
Wire Wire Line
	3500 2300 3350 2300
Wire Wire Line
	3500 2400 3350 2400
Wire Wire Line
	3500 2500 3350 2500
Wire Wire Line
	3500 3400 3350 3400
Wire Wire Line
	4900 2800 5250 2800
Wire Wire Line
	4900 3300 5050 3300
Wire Wire Line
	4900 4100 5050 4100
Wire Wire Line
	4900 4200 5050 4200
Wire Wire Line
	4900 4300 5050 4300
Text Label 3350 1800 0    50   ~ 0
R~W
Text Label 3350 1900 0    50   ~ 0
A0
Text Label 3350 2000 0    50   ~ 0
A1
Text Label 3350 2100 0    50   ~ 0
A2
Text Label 3350 2200 0    50   ~ 0
A3
Text Label 3350 2300 0    50   ~ 0
A4
Text Label 3350 2400 0    50   ~ 0
A5
Text Label 3350 2500 0    50   ~ 0
A6
Text Label 4950 4100 0    50   ~ 0
S0
Text Label 4950 4200 0    50   ~ 0
S1
Text Label 4950 4300 0    50   ~ 0
S2
Text Label 4950 3300 0    50   ~ 0
E
Text Label 4950 2800 0    50   ~ 0
~RAS0
Text Label 3350 3400 0    50   ~ 0
A15
$Comp
L power:GND #PWR0101
U 1 1 6067B748
P 7250 4650
F 0 "#PWR0101" H 7250 4400 50  0001 C CNN
F 1 "GND" H 7255 4477 50  0000 C CNN
F 2 "" H 7250 4650 50  0001 C CNN
F 3 "" H 7250 4650 50  0001 C CNN
	1    7250 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6067C205
P 9650 2550
F 0 "#PWR0102" H 9650 2300 50  0001 C CNN
F 1 "GND" H 9655 2377 50  0000 C CNN
F 2 "" H 9650 2550 50  0001 C CNN
F 3 "" H 9650 2550 50  0001 C CNN
	1    9650 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 6067C851
P 4300 4700
F 0 "#PWR0103" H 4300 4450 50  0001 C CNN
F 1 "GND" H 4305 4527 50  0000 C CNN
F 2 "" H 4300 4700 50  0001 C CNN
F 3 "" H 4300 4700 50  0001 C CNN
	1    4300 4700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 6067D587
P 7250 3150
F 0 "#PWR0104" H 7250 3000 50  0001 C CNN
F 1 "+5V" H 7265 3323 50  0000 C CNN
F 2 "" H 7250 3150 50  0001 C CNN
F 3 "" H 7250 3150 50  0001 C CNN
	1    7250 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 6067DCC0
P 4200 1400
F 0 "#PWR0105" H 4200 1250 50  0001 C CNN
F 1 "+5V" H 4215 1573 50  0000 C CNN
F 2 "" H 4200 1400 50  0001 C CNN
F 3 "" H 4200 1400 50  0001 C CNN
	1    4200 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 6067E4A0
P 9650 1550
F 0 "#PWR0106" H 9650 1400 50  0001 C CNN
F 1 "+5V" H 9665 1723 50  0000 C CNN
F 2 "" H 9650 1550 50  0001 C CNN
F 3 "" H 9650 1550 50  0001 C CNN
	1    9650 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6068131F
P 5400 2800
F 0 "R1" V 5600 2800 50  0000 C CNN
F 1 "33R" V 5500 2800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" V 5330 2800 50  0001 C CNN
F 3 "~" H 5400 2800 50  0001 C CNN
	1    5400 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 2800 5800 2800
Text Label 5600 2800 0    50   ~ 0
~RAS
Wire Wire Line
	6650 3400 6350 3400
Text Label 6350 3600 0    50   ~ 0
E
Wire Wire Line
	7050 1600 7000 1600
Wire Wire Line
	7000 1600 7000 1700
Wire Wire Line
	7000 1800 7050 1800
Wire Wire Line
	6950 1700 7000 1700
Connection ~ 7000 1700
Wire Wire Line
	7000 1700 7000 1800
Wire Wire Line
	7750 1600 7700 1600
Wire Wire Line
	7700 1600 7700 1700
Wire Wire Line
	7700 1800 7750 1800
Wire Wire Line
	7650 1700 7700 1700
Connection ~ 7700 1700
Wire Wire Line
	7700 1700 7700 1800
Wire Wire Line
	8350 1700 8700 1700
Wire Wire Line
	6350 1600 6300 1600
Wire Wire Line
	6300 1600 6300 1700
Wire Wire Line
	6300 1800 6350 1800
Wire Wire Line
	6300 1700 6100 1700
Connection ~ 6300 1700
Wire Wire Line
	6300 1700 6300 1800
Text Label 6100 1700 0    50   ~ 0
~RAS
Wire Wire Line
	7650 2500 7800 2500
Text Label 7700 2500 0    50   ~ 0
~Q
Wire Wire Line
	6650 3500 6350 3500
Wire Wire Line
	6650 3600 6350 3600
Wire Wire Line
	6650 3700 6350 3700
Wire Wire Line
	6650 3800 6350 3800
Wire Wire Line
	6650 3900 6350 3900
Wire Wire Line
	6650 4000 6350 4000
Wire Wire Line
	6650 4100 6350 4100
Wire Wire Line
	6650 4200 6350 4200
Wire Wire Line
	6650 4300 6350 4300
Wire Wire Line
	6650 4400 6350 4400
Wire Wire Line
	7850 4300 8200 4300
Wire Wire Line
	7850 3400 8200 3400
Wire Wire Line
	7850 3500 8250 3500
Text Label 7950 3400 0    50   ~ 0
~RESET
Text Label 6350 3700 0    50   ~ 0
A15
Text Label 6350 3800 0    50   ~ 0
R~W
Text Label 6350 3900 0    50   ~ 0
S0
Text Label 6350 4000 0    50   ~ 0
A0
Text Label 6350 4100 0    50   ~ 0
S1
Text Label 6350 4200 0    50   ~ 0
A1
Text Label 7950 4300 0    50   ~ 0
S2
Text Label 7950 3600 0    50   ~ 0
~DSD
Text Label 6350 4400 0    50   ~ 0
A4
Text Label 7950 4400 0    50   ~ 0
A3
Text Label 6350 4300 0    50   ~ 0
A2
Text Label 6350 3500 0    50   ~ 0
COLSEL
Text Label 7950 3500 0    50   ~ 0
Z8
NoConn ~ 7850 3700
NoConn ~ 7850 3800
NoConn ~ 7850 3900
NoConn ~ 7850 4000
NoConn ~ 7850 4100
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 606C5E3A
P 2100 4200
F 0 "#FLG0101" H 2100 4275 50  0001 C CNN
F 1 "PWR_FLAG" H 2100 4373 50  0000 C CNN
F 2 "" H 2100 4200 50  0001 C CNN
F 3 "~" H 2100 4200 50  0001 C CNN
	1    2100 4200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 606C63DE
P 1700 4200
F 0 "#FLG0102" H 1700 4275 50  0001 C CNN
F 1 "PWR_FLAG" H 1700 4373 50  0000 C CNN
F 2 "" H 1700 4200 50  0001 C CNN
F 3 "~" H 1700 4200 50  0001 C CNN
	1    1700 4200
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 606C6BA9
P 1700 4200
F 0 "#PWR0107" H 1700 4050 50  0001 C CNN
F 1 "+5V" H 1715 4373 50  0000 C CNN
F 2 "" H 1700 4200 50  0001 C CNN
F 3 "" H 1700 4200 50  0001 C CNN
	1    1700 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 606C72B3
P 2100 4200
F 0 "#PWR0108" H 2100 3950 50  0001 C CNN
F 1 "GND" H 2105 4027 50  0000 C CNN
F 2 "" H 2100 4200 50  0001 C CNN
F 3 "" H 2100 4200 50  0001 C CNN
	1    2100 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x20_Male J3
U 1 1 606C8043
P 1200 2450
F 0 "J3" H 1350 3600 50  0000 C CNN
F 1 "Conn_01x20_Male" H 1400 3500 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x20_P2.54mm_Vertical" H 1200 2450 50  0001 C CNN
F 3 "~" H 1200 2450 50  0001 C CNN
	1    1200 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x20_Male J4
U 1 1 606CDE0F
P 2100 2450
F 0 "J4" H 2250 3600 50  0000 C CNN
F 1 "Conn_01x20_Male" H 2300 3500 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x20_P2.54mm_Vertical" H 2100 2450 50  0001 C CNN
F 3 "~" H 2100 2450 50  0001 C CNN
	1    2100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 1550 1600 1550
Wire Wire Line
	1400 1650 1600 1650
Wire Wire Line
	1400 1750 1600 1750
Wire Wire Line
	1400 1850 1600 1850
Wire Wire Line
	1400 1950 1600 1950
Wire Wire Line
	1400 2050 1600 2050
Wire Wire Line
	1400 2150 1600 2150
Wire Wire Line
	1400 2250 1600 2250
Wire Wire Line
	1400 2350 1600 2350
Wire Wire Line
	1400 2450 1600 2450
Wire Wire Line
	1400 2550 1600 2550
Wire Wire Line
	1400 2650 1600 2650
Wire Wire Line
	1400 2750 1600 2750
Wire Wire Line
	1400 2850 1600 2850
Wire Wire Line
	1400 2950 1600 2950
Wire Wire Line
	1400 3050 1600 3050
Wire Wire Line
	1400 3150 1600 3150
Wire Wire Line
	1400 3250 1600 3250
Wire Wire Line
	1400 3350 1600 3350
Wire Wire Line
	1400 3450 1500 3450
Wire Wire Line
	2300 3450 2500 3450
Wire Wire Line
	2300 3350 2500 3350
Wire Wire Line
	2300 3250 2500 3250
Wire Wire Line
	2300 3150 2500 3150
Wire Wire Line
	2300 3050 2500 3050
Wire Wire Line
	2300 2950 2500 2950
Wire Wire Line
	2300 2850 2500 2850
Wire Wire Line
	2300 2750 2500 2750
Wire Wire Line
	2300 2650 2500 2650
Wire Wire Line
	2300 2550 2500 2550
Wire Wire Line
	2300 2450 2500 2450
Wire Wire Line
	2300 2350 2500 2350
Wire Wire Line
	2300 2250 2500 2250
Wire Wire Line
	2300 2150 2500 2150
Wire Wire Line
	2300 2050 2500 2050
Wire Wire Line
	2300 1950 2500 1950
Wire Wire Line
	2300 1850 2500 1850
Wire Wire Line
	2300 1750 2500 1750
Wire Wire Line
	2300 1650 2500 1650
Wire Wire Line
	2300 1550 2500 1550
Text Label 1450 2950 0    50   ~ 0
R~W
Text Label 1450 3050 0    50   ~ 0
A0
Text Label 1450 3150 0    50   ~ 0
A1
Text Label 1450 3250 0    50   ~ 0
A2
Text Label 1450 3350 0    50   ~ 0
A3
$Comp
L power:GND #PWR0109
U 1 1 6079C95D
P 1500 3450
F 0 "#PWR0109" H 1500 3200 50  0001 C CNN
F 1 "GND" H 1505 3277 50  0000 C CNN
F 2 "" H 1500 3450 50  0001 C CNN
F 3 "" H 1500 3450 50  0001 C CNN
	1    1500 3450
	1    0    0    -1  
$EndComp
Text Label 1450 2650 0    50   ~ 0
~RAS0
Text Label 2400 3450 0    50   ~ 0
A4
Text Label 2400 3350 0    50   ~ 0
A5
Text Label 2400 3250 0    50   ~ 0
A6
Wire Wire Line
	3500 2600 3350 2600
Wire Wire Line
	3500 2700 3350 2700
Wire Wire Line
	3500 2800 3350 2800
Wire Wire Line
	3500 2900 3350 2900
Wire Wire Line
	3500 3000 3350 3000
Wire Wire Line
	3500 3100 3350 3100
Wire Wire Line
	3500 3200 3350 3200
Wire Wire Line
	3500 3300 3350 3300
Wire Wire Line
	3500 3700 3350 3700
Wire Wire Line
	3500 4300 3350 4300
Wire Wire Line
	4900 3900 5050 3900
Wire Wire Line
	4900 3800 5050 3800
Wire Wire Line
	4900 3700 5050 3700
Wire Wire Line
	4900 3400 5050 3400
Wire Wire Line
	4900 3000 5050 3000
Wire Wire Line
	4900 2900 5050 2900
Wire Wire Line
	4900 2700 5050 2700
Wire Wire Line
	4900 2400 5050 2400
Wire Wire Line
	4900 2300 5050 2300
Wire Wire Line
	4900 2200 5050 2200
Wire Wire Line
	4900 2100 5050 2100
Wire Wire Line
	4900 2000 5050 2000
Wire Wire Line
	4900 1900 5050 1900
Wire Wire Line
	4900 1800 5050 1800
Text Label 4950 1800 0    50   ~ 0
Z0
Text Label 4950 1900 0    50   ~ 0
Z1
Text Label 4950 2000 0    50   ~ 0
Z2
Text Label 4950 2100 0    50   ~ 0
Z3
Text Label 4950 2200 0    50   ~ 0
Z4
Text Label 4950 2300 0    50   ~ 0
Z5
Text Label 4950 2400 0    50   ~ 0
Z6
Text Label 4950 2700 0    50   ~ 0
Z7
Text Label 4950 2900 0    50   ~ 0
~CAS
Text Label 4950 3000 0    50   ~ 0
~WE
Text Label 4950 3400 0    50   ~ 0
Q
Text Label 4950 3700 0    50   ~ 0
VCLK
Text Label 4950 3800 0    50   ~ 0
DA0
Text Label 4950 3900 0    50   ~ 0
~HS
Text Label 3350 2600 0    50   ~ 0
A7
Text Label 3350 2700 0    50   ~ 0
A8
Text Label 3350 2800 0    50   ~ 0
A9
Text Label 3350 2900 0    50   ~ 0
A10
Text Label 3350 3000 0    50   ~ 0
A11
Text Label 3350 3100 0    50   ~ 0
A12
Text Label 3350 3200 0    50   ~ 0
A13
Text Label 3350 3300 0    50   ~ 0
A14
Text Label 3350 3700 0    50   ~ 0
X2
Text Label 3350 4300 0    50   ~ 0
X1
Text Label 1450 1550 0    50   ~ 0
A11
Text Label 1450 1650 0    50   ~ 0
A10
Text Label 1450 1750 0    50   ~ 0
A9
Text Label 1450 1850 0    50   ~ 0
A8
Text Label 1450 1950 0    50   ~ 0
X2
Text Label 1450 2050 0    50   ~ 0
X1
Text Label 1450 2150 0    50   ~ 0
VCLK
Text Label 1450 2250 0    50   ~ 0
DA0
Text Label 1450 2350 0    50   ~ 0
~HS
Text Label 1450 2450 0    50   ~ 0
~WE
Text Label 1450 2550 0    50   ~ 0
~CAS
Text Label 1450 2750 0    50   ~ 0
Q
Text Label 1450 2850 0    50   ~ 0
E
Text Label 2400 3150 0    50   ~ 0
A7
Text Label 2400 3050 0    50   ~ 0
S2
Text Label 2400 2950 0    50   ~ 0
S1
Text Label 2400 2850 0    50   ~ 0
S0
Text Label 2400 2750 0    50   ~ 0
Z0
Text Label 2400 2650 0    50   ~ 0
Z1
Text Label 2400 2550 0    50   ~ 0
Z2
Text Label 2400 2450 0    50   ~ 0
Z3
Text Label 2400 2350 0    50   ~ 0
Z4
Text Label 2400 2250 0    50   ~ 0
Z5
Text Label 2400 2150 0    50   ~ 0
Z6
Text Label 2400 2050 0    50   ~ 0
Z7
Text Label 2350 1950 0    50   ~ 0
A15
Text Label 2350 1850 0    50   ~ 0
A14
Text Label 2350 1750 0    50   ~ 0
A13
Text Label 2350 1650 0    50   ~ 0
A12
$Comp
L power:+5V #PWR0110
U 1 1 60844A01
P 2500 1550
F 0 "#PWR0110" H 2500 1400 50  0001 C CNN
F 1 "+5V" V 2500 1750 50  0000 C CNN
F 2 "" H 2500 1550 50  0001 C CNN
F 3 "" H 2500 1550 50  0001 C CNN
	1    2500 1550
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 6085C872
P 8400 3500
F 0 "R2" V 8600 3500 50  0000 C CNN
F 1 "33R" V 8500 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P5.08mm_Vertical" V 8330 3500 50  0001 C CNN
F 3 "~" H 8400 3500 50  0001 C CNN
	1    8400 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8550 3500 8900 3500
Text Label 8600 3500 0    50   ~ 0
Z8_OUT
$Comp
L Device:C C1
U 1 1 6067EA8F
P 5750 3850
F 0 "C1" H 5865 3896 50  0000 L CNN
F 1 "100n" H 5865 3805 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5788 3700 50  0001 C CNN
F 3 "~" H 5750 3850 50  0001 C CNN
	1    5750 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 6067F0EB
P 10400 2050
F 0 "C2" H 10515 2096 50  0000 L CNN
F 1 "100n" H 10515 2005 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 10438 1900 50  0001 C CNN
F 3 "~" H 10400 2050 50  0001 C CNN
	1    10400 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 606808E9
P 10400 1900
F 0 "#PWR0111" H 10400 1750 50  0001 C CNN
F 1 "+5V" H 10415 2073 50  0000 C CNN
F 2 "" H 10400 1900 50  0001 C CNN
F 3 "" H 10400 1900 50  0001 C CNN
	1    10400 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 60680D8B
P 10400 2200
F 0 "#PWR0112" H 10400 1950 50  0001 C CNN
F 1 "GND" H 10405 2027 50  0000 C CNN
F 2 "" H 10400 2200 50  0001 C CNN
F 3 "" H 10400 2200 50  0001 C CNN
	1    10400 2200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 60681118
P 5750 3700
F 0 "#PWR0113" H 5750 3550 50  0001 C CNN
F 1 "+5V" H 5765 3873 50  0000 C CNN
F 2 "" H 5750 3700 50  0001 C CNN
F 3 "" H 5750 3700 50  0001 C CNN
	1    5750 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 606815F0
P 5750 4000
F 0 "#PWR0114" H 5750 3750 50  0001 C CNN
F 1 "GND" H 5755 3827 50  0000 C CNN
F 2 "" H 5750 4000 50  0001 C CNN
F 3 "" H 5750 4000 50  0001 C CNN
	1    5750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 3400 9450 3400
Wire Wire Line
	9750 3500 9450 3500
Wire Wire Line
	9750 3850 9550 3850
$Comp
L power:GND #PWR0115
U 1 1 606A5749
P 9550 3850
F 0 "#PWR0115" H 9550 3600 50  0001 C CNN
F 1 "GND" H 9555 3677 50  0000 C CNN
F 2 "" H 9550 3850 50  0001 C CNN
F 3 "" H 9550 3850 50  0001 C CNN
	1    9550 3850
	1    0    0    -1  
$EndComp
Text Label 9450 3500 0    50   ~ 0
Z8_OUT
Text Label 9450 3400 0    50   ~ 0
~RESET
$Comp
L Device:CP C3
U 1 1 6069C675
P 2750 4200
F 0 "C3" H 2868 4246 50  0000 L CNN
F 1 "4u7" H 2868 4155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2788 4050 50  0001 C CNN
F 3 "~" H 2750 4200 50  0001 C CNN
	1    2750 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 6069EBD4
P 2750 4350
F 0 "#PWR0116" H 2750 4100 50  0001 C CNN
F 1 "GND" H 2755 4177 50  0000 C CNN
F 2 "" H 2750 4350 50  0001 C CNN
F 3 "" H 2750 4350 50  0001 C CNN
	1    2750 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 6069EF9E
P 2750 4050
F 0 "#PWR0117" H 2750 3900 50  0001 C CNN
F 1 "+5V" H 2765 4223 50  0000 C CNN
F 2 "" H 2750 4050 50  0001 C CNN
F 3 "" H 2750 4050 50  0001 C CNN
	1    2750 4050
	1    0    0    -1  
$EndComp
Text Notes 6150 1100 0    50   ~ 0
Approx. 30-75ns delay ensures row address hold time after ~RAS
Text Notes 6150 1250 0    50   ~ 0
A 74LS00 may also be used.
Text Label 8400 1700 0    50   ~ 0
COLSEL
Wire Wire Line
	7850 3600 8200 3600
Text Label 6350 3400 0    50   ~ 0
~Q
Wire Wire Line
	7050 2400 7000 2400
Wire Wire Line
	7000 2400 7000 2500
Wire Wire Line
	7000 2600 7050 2600
Wire Wire Line
	7000 2500 6800 2500
Connection ~ 7000 2500
Wire Wire Line
	7000 2500 7000 2600
Text Label 6850 2500 0    50   ~ 0
Q
Wire Wire Line
	7850 4400 8200 4400
NoConn ~ 7850 4200
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 6077126C
P 9950 3400
F 0 "J1" H 9900 3400 50  0000 R CNN
F 1 "Conn_01x02_Male" H 9900 3300 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9950 3400 50  0001 C CNN
F 3 "~" H 9950 3400 50  0001 C CNN
	1    9950 3400
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 60771D7B
P 9950 3750
F 0 "J2" H 9900 3750 50  0000 R CNN
F 1 "Conn_01x02_Male" H 9900 3650 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9950 3750 50  0001 C CNN
F 3 "~" H 9950 3750 50  0001 C CNN
	1    9950 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9750 3750 9450 3750
Text Label 9450 3750 0    50   ~ 0
~DSD
Text Notes 8950 4500 0    50   ~ 0
DSD is intended to be an open drain style of output\nto be connected to the main board to disable devices\nwhile bank registers are written.\n(Usually LS138 pin 6 on Dragon machines)
Text Notes 6600 2100 0    50   ~ 0
Registers written on falling edge of q
Text Notes 650  1100 0    50   ~ 0
Banker board plugs into 74LS783/785 socket on main board.\n
Text Notes 3750 6950 0    50   ~ 0
Theory of operation:\n\nVideo address is selected when E clock is low.\nMPU address is selected when E clock is high.\nA15 selects which bank register is used for MPU address. (Lower 32K or upper 32K)\nA delayed version of RAS provides the row/column select signal COLSEL.\nCOLSEL selects which part of address is routed to Z8.\n4 x lower 32K banks + 4 x upper 32K banks = 256K.\n\nBank registers are written on falling edge of Q.\nBank registers based at $ff30 for very simple address decode.\nDSD signal is used to disable device at $ff20-$ff3f on main board.\n\nUsable addresses are restricted for the faster MPU rate settings.\nAddress dependent rate has E high during video column address.\n(Meaning the Z8 column bits of the MPU and video addresses need to\nbe the same to avoid corrupted video display)\nFast rate has E low during MPU row address.\n(Meaning the Z8 row bits of the MPU and video addresses need to\nbe the same to avoid crashes)\n\n\n
Text Notes 3500 1100 0    50   ~ 0
74LS785 required for 256 row refresh\n
Text Notes 950  7100 0    50   ~ 0
Proposed register map:\n\nWrite any value to address to perform indicated function\n\n$ff30-$ff33 Set lower 32K bank\n\n  $ff30  Set lower 32K bank 0 *\n  $ff31  Set lower 32K bank 1\n  $ff32  Set lower 32K bank 2\n  $ff33  Set lower 32K bank 3\n\n$ff34-$ff37 Set upper 32K bank\n\n  $ff34  Set upper 32K bank 0 *\n  $ff35  Set upper 32K bank 1\n  $ff36  Set upper 32K bank 2\n  $ff37  Set upper 32K bank 3\n\n$ff38-$ff39 Set video bank mode\n\n  $ff38  Set video bank number = lower 32K bank number *\n  $ff39  Set video bank number = 0\n\n* = power up / reset state
$EndSCHEMATC

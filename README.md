## Dragon 256K Banker Board

>Cram it with RAM. 256K Memory Upgrade!

Or at least that's what it said on the cover of the September 1985 edition of Hot Coco.

The article within described a circuit providing access to 256K of memory in 32K banks. The memory ICs are fitted on the main board in place of the existing ICs. The design exploits the fact that 41256 DRAMs are pin-compatible with 4164 DRAMs, with just one extra address line (Z8) required to generate and route to the new ICs. The circuit occupies a small PCB that plugs into the SAM socket.

This is a revisit of that old design with a few tweaks and improvements while still using 1980's technology.

The new board is smaller than the old one so it should still be compatible with CoCo 1 and 2 machines, but it is advisable to first check if the board will fit.

### Project Status

The PCB design has been successfully tested with 256K DRAMs in a MKII Dragon 32 that was originally upgraded to 64K.

Also tested on the older Dragon 32 version that has jumpers near the power connector.

### Prerequisites

* A 64K machine such as a Dragon 64 or Dragon 32 upgraded to 64K.
* 8 x 41256 DRAMs to replace the existing memory.
* A 74LS785 SAM2. Optional but guarantees refresh within specification*
* A GAL22V10 or ATF22V10 and a means of programming it. (TL866ii works for me)
* A 74LS00 or 74LS132 quad dual input NAND
* Some soldering and PCB prototype/rework skills.

\* It is possible to get away with a 74LS783 SAM as video display accesses may provide sufficient refresh.

This can be made to work starting with a 32K machine, but there is more work involved to reroute the DRAM signals. There is an article for upgrading Dragon 32 machines to 64K [here](http://archive.worldofdragon.org/index.php?title=Dragon_32_-_64K_Upgrade). One bonus is that the 'decoder mod' shouldn't be necessary when using the 74LS785.

Dragon 32 machines come in different versions but it is quite likely that there is a PCB trace already connecting pin 1 of each memory IC together. This needs to be isolated from the rest of the circuitry and any bypass capacitors removed so that it can be used to supply the new address signal.

Some 32K machines have a 16K memory daughterboard. This will need to be removed to make room for the banker board.

Dragon 64 machines connect pin 1 of each memory IC to +5V. Rather than modify the motherboard, it may be preferable to bend up pin 1 of each memory IC and connect them together with short flying wires.

The circuit is not too difficult to make on a protoboard, particularly the type that is arranged for DIL packages. I picked up some low cost boards from ebay, which turned out to be RKpt3 boards originally from [here](https://www.rkeducation.co.uk/rkeducation-5x7cm-pcb-copper-matrix-prototyping-stripboard-2878-p.asp). 

### PCB assembly notes

* Some 40 way IC sockets have features that will interfere slightly with the 20 way pin headers, even after cutting the pins down. It may be necessary to remove some material with a file to get the 40 way socket to lay flat on the board.
* Even though the ground pins have thermal relief features they can still be difficult to solder due to the ground plane taking away the heat. Preheating the board with a hot air gun can help with this.

### Installing the board

* Use antistatic precautions
* Remove the SAM from its socket on the main board and install it (or a LS785) into the corresponding socket on the banker board.
* Install the banker board in the SAM socket on the main board. It may be necessary to gently bend a capacitor or two out of the way.
* Double check the orientation of the SAM and the banker board.

#### Wiring

I would suggest using 'Dupont' style jumper wires to make it easier to remove the board in future. Miniature grabber probes are another popular option.

* Connect Z8 on the banker board to pin 1 of all 41256 memory ICs.
* Connect RST on the banker board to the active low reset signal on the main board. A convenient place is D5 anode in a Dragon 32, D10 cathode in a MKII Dragon 32 or D19 anode in a Dragon 64. If in doubt confirm the signal ends up on pin 37 of the CPU.
* Connect DSD on the banker board to pin 6 of the LS138 decoder IC on the main board. This signal modifies the normal device decoding in the Dragon allowing us to use part of the address range normally reserved for one of the PIAs. A convenient place for this signal is the 4K7 pullup resistor, usually R39 in a Dragon 32 or R71 in a Dragon 64.

### Changes from the original design

* Reduced chip count

Most of the logic has been replaced with a GAL22V10 programmable logic device, reducing the IC count from five to two.

* Simplified programming model

Unlike the original, the lower and upper 32K banks can now be selected independently. A VDG mode setting replaces the old VDG bank selection: The display can now either be in the same bank as the lower 32K bank, or it can be fixed in bank 0.

* Register addressing

The original design updated the bank registers by _reading_ addresses in the range $ffc0-$ffdf. This likely caused compatibility issues with existing software, particularly with software that updated SAM registers using the CLR instruction (Because the CLR instruction performs a read before it writes). The new design updates the registers by _writing_ to the range $ff30-$ff3f.

* DRAM timing

The original design has approximately 20ns of Z8 hold time after falling RAS. The new design increases the hold time to give more of a margin.

### Programming

The bank registers are accessed by writing any value to addresses $ff30 - $ff39

| Address  | Action                      |
|:---------|:----------------------------|
| $ff30    | Set lower 32K bank 0 *      |
| $ff31    | Set lower 32K bank 1        |
| $ff32    | Set lower 32K bank 2        |
| $ff33    | Set lower 32K bank 3        |
| $ff34    | Set upper 32K bank 0 *      |
| $ff35    | Set upper 32K bank 1        |
| $ff36    | Set upper 32K bank 2        |
| $ff37    | Set upper 32K bank 3        |
| $ff38    | vdg bank = lower 32K bank * |
| $ff39    | vdg bank = 0                |

\* Power up / reset state

The `P` bit in the SAM control register can be used to swap the upper and lower 32K pages. This can be a tricky technique for purely RAM resident software as it would be swapping out the 32K page containing the running software. The solution is to perform the swap via a code stub that is present in the same location across multiple 32K pages.

### Included files

* KiCad project files and plots for PCB manufacture
* WinCupl pld file and jed fuse map for programming the GAL22V10D
* A schematic in pdf form and a 3D visualisation of the PCB
* A memory test program and demos.

### Theory of operation

* The Z8 address line is supplied by a multiplexor
* Video bank is selected when E clock is low
* MPU bank is selected when E clock is high
* A15 selects which bank registers are used for MPU address (Lower 32K or upper 32K)
* A delayed version of RAS provides the row/column select signal COLSEL
* COLSEL selects which bit of the bank number is routed to Z8
* 4 x lower 32K banks + 4 x upper 32K banks = 256K
* Bank registers are updated on falling edge of Q
* Bank registers based at $ff30 for very simple address decode
* DSD signal is used to disable device at $ff20-$ff3f on main board

#### MPU rate limitations

Using the E clock to discriminate between MPU and video addresses mean that banks are restricted for the faster MPU rate settings.

The address dependent rate has E high during video column address. This means the Z8 column bits of the MPU and video addresses need to be the same to avoid a corrupted video display. i.e. the video and mpu should both be in banks 0-1, or both in banks 2-3.

The fast rate has E low during MPU row address. This means the Z8 row bits of the MPU and video addresses need to be the same to avoid crashes. i.e. the video and mpu should both be in banks 0 or 2, or both in banks 1 or 3.

### Acknowledgements

* Inspired by 'The Banker', a 1985 design by J & R Electronics, Columbia
